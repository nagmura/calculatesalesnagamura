package jp.alhinc.nagamura_shohei.Calculate_Sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		try {
			BufferedReader br = null;
			Map<String, String> branchMap = new HashMap<>();
			Map<String, Long> sumMap= new HashMap<>();
			try {
				File file = new File(args[0],"branch.lst");

				// エラー処理 1.1
				if (!file.exists()) {
					System.out.println("支店定義ファイル存在がしません");
					return;

				}
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String line;
				while ((line = br.readLine()) != null) {
					String[] item = line.split(",", -1);
					// 3.エラー処理 1.2
					if (!item[0].matches("^\\d{3}$") || item.length != 2) {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}

					branchMap.put(item[0], item[1]);
				}
			} finally {
				if (br != null) {
					br.close();
				}
			}

			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File file, String str) {

					File file3 = new File(file, str);

					if (str.matches("^\\d{8}.rcd$") && file3.isFile()) {
						return true;
					} else {
						return false;
					}
				}
			};

			File dir = new File(args[0]);
			File[] list = dir.listFiles(filter);

			Arrays.sort(list);
			// 3.エラー処理2.1

			String mim = list[0].getName().substring(0,8);
			int minimum = Integer.parseInt(mim);

			String max = list[list.length - 1].getName().substring(0,8);
			int maximum = Integer.parseInt(max);
			if(minimum + list.length == maximum) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}

			for (File file1 : list) {
				try {
					FileReader fr = new FileReader(file1);
					br = new BufferedReader(fr);

					String line = br.readLine();
					String line2 = br.readLine();
					Long total = Long.parseLong(line2);

					String line3 = br.readLine();
					// 3.エラー処理2.4
					if (line3 != null) {
						System.out.println(file1.getName()+"のフォーマットが不正です");
						return;
					}
					// 3.エラー処理 2.3
				if (!branchMap.containsKey(line)) {
						System.out.println(file1.getName()+"の支店コードが不正です");
						return;
					}
					// 3.エラー処理 2.2
					if (Math.abs(total) > 9_999_999_999L) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					if (sumMap.containsKey(line)) {
						total = total + sumMap.get(line);
						sumMap.put(line, total);
					} else {
						sumMap.put(line, total);
					}

				} finally {
					if (br != null) {
						br.close();
					}
				}
			}
			PrintWriter pw = null;
			try {
				File file = new File(args[0], "branch.out");
				pw = new PrintWriter(file);
				for (String key : branchMap.keySet()) {
					pw.println(key + "," + branchMap.get(key) + "," + sumMap.get(key));
				}
			} finally {
				pw.close();
			}
		} catch (Exception e) {
			System.out.println("予期せぬエラーが発生しました");
		}
	}
}
